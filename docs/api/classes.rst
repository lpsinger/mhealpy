Classes
=======

HealpixBase
-----------

.. autoclass:: mhealpy.HealpixBase
   :show-inheritance:
   :members:
   :inherited-members:

HealpixMap
----------
      
.. autoclass:: mhealpy.HealpixMap
   :show-inheritance:
   :members:
   :inherited-members:

